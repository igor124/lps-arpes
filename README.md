# LPS ARPES

These procedures are meant to read a series of ARPES 2D files and create a 3D wave that can be displayed in a set of two windows. An example of how they work can be found in the Igor file : Example_ARPES_LPS. The graph « ProcessedFiles » show processed ARPES data as a function of theta (theta can be changed on top left box), the graph « FS_k_ProcessedFiles » show the corresponding Fermi Surface (the red line indicate the current theta value). 

The procedures were written by Véronique Brouet (LPS Orsay). They integrate the ImageTool window written by Jonathan Denlinger (ALS - jddenlinger@lbl.gov) and a procedure to load text file written by Felix Baumberger (U. Geneva - felix.baumberger@unige.ch). Some functionalities are specific to Cassiopée beamline from SOLEIL (reading files from an automatic run for example) but most could be adapted to any data. 

For questions, write to : veronique.brouet@u-psud.fr

-----------------------------------------

## Getting started
Open the “ARPES_menu” procedure. It will call the other procedures and create a menu “ARPES” in Igor menu.


## Load Scienta data text files
Just "drag" the file (1D or 2D) into Igor. The file is transferred to the "OriginalData" folder.

NB: This is treated by "LoadTextFiles" procedure. The default setting is for Cassiopee, this can be adapted easily for another beamline. 


## Building a 3D wave from a series of 2D files
From the menu, open the "Process Images" panel. 

All 2D files should be in one folder. They will be cropped and normalized according to the parameters in this window. They will then be compiled in a 3D wave, saved as “tmp_3D” in the folder specified as “Folder to save results”. 

This can work for any series of 2D files, made as a function of theta, phi angle, energy or temperature : select the corresponding “mode”. 

Experimental parameters are loaded automatically for Cassiopée runs, by clicking on “Set experimental parameters”. Otherwise, they have to be entered manually in the “2D Original Information Table”.

When all parameters are set, click “Process all images” to build and view the 3D wave.


## Viewing a 3D wave
This can work for any 3D wave called tmp_3D and saved in a folder (anything but root). 

Choose “Window Plot of 3D wave” from menu and pick up the right folder. This will build the two windows. 


## Other functionalities
There are many other functionalities that can be guessed from the menu or pannels. 

- Clicking on “Go to stacks” of the ProcessedFiles window loads the current image in ImageTool, where stacks, normalization, averaging, etc., can be done. This window is also available from the menu.
- Automatic fitting of MDC or EDC can be obtained from the menu. This works on the image displayed in ImageTool. 


Credits
-------
Veronique Brouet / LPS
